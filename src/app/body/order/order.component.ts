import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConstService } from 'src/app/services/const.service';
import { MatDialogRef } from '@angular/material';
import { OrderService } from 'src/app/services/order.service';
import { ProcessService } from 'src/app/services/process.service';
import { ToastrService } from 'ngx-toastr';
import { Order } from 'src/app/shared/models/order';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  subscriptions: Subscription[] = [];
  customerForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<OrderComponent>, private _formBuilder: FormBuilder, private CONST: ConstService, private orderService: OrderService, private processService: ProcessService, private toastrService: ToastrService ) {

  }

  selectionChange(change){
    if (change.selectedIndex == 2){
      this.onOrder();
    }
  }

  onPressNumber(event: any){
    if(Number.parseInt(event.data) == NaN){
      event.stopPropagation();
    }
  }

  setProperties() {
    (<HTMLElement>document.getElementsByClassName('mat-dialog-container')[0]).style.padding = '0';
  }

  ngOnInit() {
    this.setProperties();
    this.customerForm = this._formBuilder.group({
      customerName: ['', Validators.required],
      email: ['', Validators.email],
      address: ['', Validators.required],
      phoneNumber: ['', Validators.required]
    });
  }

  onOrder(){
    let productsOb = {
      products: this.CONST.getCart
    }

    let orderForm: Order = Object.assign(this.customerForm.value, productsOb);

    let id: number = this.processService.addTask();
    this.subscriptions.push(this.orderService.onOrder(orderForm).subscribe(() => {
      this.CONST.clearCart();
      this.toastrService.success("Đặt hàng thành công!", `ĐẶT HÀNG`, {
        timeOut: 2000
      });
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  onClose(){
    this.dialogRef.close();
  }
}
