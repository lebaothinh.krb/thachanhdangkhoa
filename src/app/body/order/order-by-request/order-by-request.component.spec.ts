import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderByRequestComponent } from './order-by-request.component';

describe('OrderByRequestComponent', () => {
  let component: OrderByRequestComponent;
  let fixture: ComponentFixture<OrderByRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderByRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderByRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
