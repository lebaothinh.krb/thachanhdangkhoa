import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrderService } from 'src/app/services/order.service';
import { ProcessService } from 'src/app/services/process.service';
import { OrderByRequest } from 'src/app/shared/models/order';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-order-by-request',
  templateUrl: './order-by-request.component.html',
  styleUrls: ['./order-by-request.component.css']
})
export class OrderByRequestComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  subscriptions: Subscription[] = [];

  constructor(public dialogRef: MatDialogRef<OrderByRequestComponent>, private _formBuilder: FormBuilder, private orderService: OrderService, private processService: ProcessService, private toastrService: ToastrService) { }

  formRequest: FormGroup;
  describeProduct: string = '';

  ngOnInit() {
    this.formRequest = this._formBuilder.group({
      customerName: ['', Validators.required],
      email: ['', Validators.email],
      address: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      describeProduct: ['']
    });
  }

  onClose() {
    this.dialogRef.close();
  }

  sendRequest() {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.orderService.onOrderByRequest(<OrderByRequest>this.formRequest.value).subscribe(() => {
      this.toastrService.success("Đặt hàng thành công!", `ĐẶT HÀNG`, {
        timeOut: 2000
      });
      this.processService.removeTask(id);
      this.onClose();
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  onChange(e) {
    this.formRequest.controls.describeProduct.setValue(e);
  }
}
