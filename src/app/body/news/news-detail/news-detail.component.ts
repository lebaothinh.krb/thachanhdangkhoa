import { Component, OnInit, ViewChild, ViewContainerRef, AfterContentInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { News } from 'src/app/shared/models/news';
import { NewsService } from 'src/app/services/news.service';
import { ProcessService } from 'src/app/services/process.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.css']
})
export class NewsDetailComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute, private newsService: NewsService, private processService: ProcessService, private toastrService: ToastrService) {
  }
  window: any;
  news = new News(`Boxing icon has the will for a couple more fights
  ssds`, `The highly anticipated world championship fight will take place at 10am and is the second major boxing blockbuster in the nation after 43 years`, 'bg2.jpg', `<p>The highly anticipated world championship fight will take place at 10am and is the
  second major boxing blockbuster in the nation after 43 years</p>`);

  ngOnInit() {
    this.subscriptions.push(this.route.params.subscribe((param) => {
      this.getNewsDetail(param['id']);
    }));
    this.onSetSocial();
    (<any>window).FB.XFBML.parse();
  }


  getNewsDetail(idNews: number) {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.newsService.getDetailNews(idNews).subscribe((news: News) => {
      this.news = news;
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  onSetSocial() {
    const elFB = document.getElementsByClassName('fb-like');
    for (let i = 0 ; i< elFB.length ; i++){
      elFB.item(i).setAttribute('data-href',window.location.href)
    }
  }
}
