import { Component, OnInit, OnDestroy } from '@angular/core';
import { News } from 'src/app/shared/models/news';
import { ProcessService } from 'src/app/services/process.service';
import { NewsService } from 'src/app/services/news.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  subscriptions: Subscription[] = [];
  currentScrool = 0;

  constructor(private processService: ProcessService, private newsService: NewsService, private toastrService: ToastrService) { }

  newes: Array<News> = [];

  ngOnInit() {
    this.getAllNewes(this.currentScrool);
  }

  onScrollDown(): void {
    this.currentScrool++;
    this.getAllNewes(this.currentScrool);
  }

  getAllNewes(index: number) {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.newsService.getAllNewes(index).subscribe((newes: Array<News>) => {
      newes.forEach(n => {
        n.postedDate = new Date(n.postedDate)
      })
      this.newes = this.newes.concat(newes);
      console.log("get sp")
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }))
  }

  // getNewes() {
  //   let id: number = this.processService.addTask();
  //   this.subscriptions.push(this.newsService.getNewes().subscribe((newes: Array<News>) => {
  //     this.newes = newes;
  //     this.newes.forEach(n => {
  //       n.postedDate = new Date(n.postedDate)
  //     })
  //     this.processService.removeTask(id);
  //   }, error => {
  //     if (error) {
  //       this.toastrService.error(error.error.Message, `Lỗi`, {
  //         timeOut: 2000
  //       });
  //     }
  //     else {
  //       this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
  //         timeOut: 2000
  //       });
  //     }
  //     this.processService.removeTask(id);
  //   }));
  // }

}
