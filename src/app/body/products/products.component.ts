import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Product, Topic } from 'src/app/shared/models/product';
import { Subscription } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { ProcessService } from 'src/app/services/process.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';


export class SearchingData {
  searchingKey: string;
  topic: number;
  type: number;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {


  constructor(private route: ActivatedRoute, private productService: ProductService, private processService: ProcessService, private toastrService: ToastrService) { }

  subscriptions: Subscription[] = [];
  currentScrool = 0;
  isParam = false;
  products: Array<Product> = []


  onLoadData() {
    this.subscriptions.push(this.route.params.subscribe((params) => {
      if (Object.keys(params).length == 0) {
        this.getAllProducts(this.currentScrool);
        this.isParam = false;
      }
      else {
        this.getProduct(<SearchingData>params);
        this.isParam = true;
      }
    }));
  }

  onScrollDown(): void {
    if (!this.isParam) {
      this.currentScrool++;
      this.getAllProducts(this.currentScrool);
    }
  }

  getAllProducts(index: number) {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.productService.getAllProducts(index).subscribe((products: Array<Product>) => {
      this.products = this.products.concat(products);
      console.log("get sp")
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }))
  }

  getProduct(search: SearchingData = {
    searchingKey: '',
    topic: 1,
    type: 1
  }) {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.productService.getProduct(search.searchingKey, search.topic, search.type).subscribe((products: Array<Product>) => {
      this.products = products;
      this.processService.removeTask(id);
      console.log("get sp by key")
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  ngOnInit() {
    this.currentScrool = 0;
    this.onLoadData();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }


}
