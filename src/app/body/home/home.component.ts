import { Component, OnInit } from '@angular/core';
import { ConstService } from 'src/app/services/const.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private CONST: ConstService) { }
  
  ngOnInit() {
  }

}
