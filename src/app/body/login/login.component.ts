import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ProcessService } from 'src/app/services/process.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  constructor(public dialogRef: MatDialogRef<LoginComponent>, private router: Router, private userService: UserService, private toastrService: ToastrService, private processService: ProcessService) { }

  subscriptions: Subscription[] = [];
  form: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  error: string | null;

  ngOnInit() {
  }

  onClose() {
    this.dialogRef.close();
  }

  onSetToken(token: string){
    if (localStorage.getItem('jwt')){
      localStorage.removeItem('jwt');
    }
    localStorage.setItem('jwt',token);
  }

  onLogin() {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.userService.signIn(this.form.value).subscribe((result: any) => {
      this.router.navigateByUrl('/admin');
      this.onClose();
      this.onSetToken(result.token);
      this.processService.removeTask(id);
      this.toastrService.success("Đăng Nhập Thành Công", `Thành Công`, {
        timeOut: 2000
      });
      this.router.navigate(["/admin"]);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

}
