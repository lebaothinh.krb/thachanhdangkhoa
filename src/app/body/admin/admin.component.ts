import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConstService } from 'src/app/services/const.service';
import { CropImageComponent } from './shared/crop-image/crop-image.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private CONST: ConstService, private matDiaLog: MatDialog) { }

  ngOnInit() {
  }

  openCropImage(){
    this.matDiaLog.open(CropImageComponent)
  }
}
