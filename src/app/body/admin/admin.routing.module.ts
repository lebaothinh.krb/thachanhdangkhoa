import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ManageProductsComponent } from './components/manage-products/manage-products.component';
import { ManageNewsComponent } from './components/manage-news/manage-news.component';
import { ManageOrdersComponent } from './components/manage-orders/manage-orders.component';
import { SendInfoComponent } from './components/send-info/send-info.component';
import { PageNotFoundComponent } from 'src/app/shared/components/page-not-found/page-not-found.component';
import { ManageHomeComponent } from './components/manage-home/manage-home.component';
import { ManageTopicTypeComponent } from './components/manage-topic-type/manage-topic-type.component';

const adminRoutes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            {
                path: '',
                redirectTo: 'manage-home',
                pathMatch: 'full'
            },
            { path: 'manage-home', component: ManageHomeComponent },
            { path: 'manage-orders', component: ManageOrdersComponent },
            { path: 'manage-products', component: ManageProductsComponent },
            { path: 'manage-news', component: ManageNewsComponent },
            { path: 'send-info', component: SendInfoComponent },
            { path: 'manage-topic-type', component: ManageTopicTypeComponent },
            { path: '**' , component: PageNotFoundComponent}
        ]
    }
];

export const AdminRouter = RouterModule.forChild(adminRoutes);
