import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { ManageProductsComponent } from './components/manage-products/manage-products.component';
import { ManageNewsComponent } from './components/manage-news/manage-news.component';
import { ManageOrdersComponent } from './components/manage-orders/manage-orders.component';
import { SendInfoComponent } from './components/send-info/send-info.component';
import { MatButtonModule, MatTableModule, MatSelectModule, MatFormFieldModule, MatInputModule, MatIconModule, MatTabsModule, MatListModule } from '@angular/material';
import { AdminRouter } from './admin.routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/modules/shared/shared.module';
import { ManageHomeComponent } from './components/manage-home/manage-home.component';
import { AddingProductComponent } from './shared/adding-product/adding-product.component';
import { ManageTopicTypeComponent } from './components/manage-topic-type/manage-topic-type.component';

@NgModule({
  declarations: [
    AdminComponent, 
    ManageProductsComponent, 
    ManageNewsComponent, 
    ManageOrdersComponent, 
    SendInfoComponent, 
    ManageHomeComponent, 
    ManageTopicTypeComponent,
  ],
  imports: [
    AdminRouter,
    MatTableModule,
    SharedModule,
    MatTabsModule,
    MatListModule,
  ]
})
export class AdminModule { }