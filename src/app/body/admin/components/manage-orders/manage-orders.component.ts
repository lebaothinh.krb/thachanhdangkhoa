import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConstService } from 'src/app/services/const.service';
import { Order, OrderByRequest, OrderStatus } from 'src/app/shared/models/order';
import { Product, Topic } from 'src/app/shared/models/product';
import { ProcessService } from 'src/app/services/process.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { OrderService } from 'src/app/services/order.service';
import { MatDialog } from '@angular/material';
import { AddingProductComponent } from '../../shared/adding-product/adding-product.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-manage-orders',
  templateUrl: './manage-orders.component.html',
  styleUrls: ['./manage-orders.component.css']
})
export class ManageOrdersComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  subscriptions: Subscription[] = [];

  constructor(private fb: FormBuilder, private processService: ProcessService, private toastrService: ToastrService, private orderService: OrderService, private matDiaLog: MatDialog) { }

  formUpdate: FormGroup;
  isUpdate: boolean = true;
  isOrder: boolean = true;
  status = [
    {
      displayName: 'Chưa Xem',
      value: 0
    },
    {
      displayName: 'Đã Xem',
      value: 1
    },
    {
      displayName: 'Đang Xử Lý',
      value: 2
    },
    {
      displayName: 'Hoàn Thành',
      value: 3
    }
  ];

  selectedFile: File;
  selectedFileName: string = '';
  isFileSelected = false;

  countOrderNF = 0;
  countOrderByRequestNF = 0;

  displayedColumnsOrders = ['id', 'customerName', 'email', 'phoneNumber', 'address', 'products', 'orderDate', 'status', 'deleting', 'updating'];
  displayedColumnsOrdersByRequest = ['id', 'customerName', 'email', 'phoneNumber', 'address', 'describe', 'orderDate', 'status', 'deleting', 'updating'];

  orders: Array<Order> = []

  ordersByRequest: Array<OrderByRequest> = []

  ngOnInit() {
    this.getOrders();
    this.getOrdersByRequest();
  }

  countOrder() {
    this.countOrderNF = this.orders.filter(o => o.status != 3).length;
  }

  countOrderByRequest() {
    this.countOrderByRequestNF = this.ordersByRequest.filter(o => o.status != 3).length;
  }

  getOrders() {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.orderService.getOrdersByAdmin().subscribe((orders: Array<Order>) => {
      this.orders = orders;
      this.countOrder();
      console.log(this.orders)
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        console.log(error)
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  getOrdersByRequest() {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.orderService.getOrdersByRequestByAdmin().subscribe((orders: Array<OrderByRequest>) => {
      this.ordersByRequest = orders;
      this.countOrderByRequest();
      console.log(this.ordersByRequest)
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  onChange(event) {
    this.formUpdate.controls.describeProduct.setValue(event);
  }

  removeProduct(product) {
    let newArr = (<Array<Product>>this.formUpdate.value['products']).filter(i => i.id != product.id);
    this.formUpdate.controls.products.setValue(newArr);
  }

  openDialogAddProduct() {
    this.subscriptions.push(
      this.matDiaLog.open(AddingProductComponent, {
        width: '100vh',
        data: this.formUpdate.value['products']
      }).afterClosed().subscribe((product: Product) => {
        if (product) {
          let products = Array.from(this.formUpdate.value['products']);
          products.push(product);
          this.formUpdate.controls.products.setValue(products);
        }
      })
    );
  }

  delete(element: any) {
    this.matDiaLog.open(ConfirmDialogComponent, {
      data: "Bạn Có Muốn Xóa Đơn Hàng Này Không?"
    }).afterClosed().subscribe((result: boolean) => {
      if (result) {
        console.log(result)
        if (element.orderId) {
          let id: number = this.processService.addTask();
          this.subscriptions.push(this.orderService.deleteOrderByAdmin(element.orderId).subscribe(() => {
            this.orders = this.orders.filter(o => o.orderId != element.orderId);
            this.toastrService.success("Xóa Thành Công", "Thành Công", { timeOut: 2000 });
            this.processService.removeTask(id);
            this.countOrder();
          }, error => {
            if (error) {
              this.toastrService.error(error.error.Message, `Lỗi`, {
                timeOut: 2000
              });
            }
            else {
              this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
                timeOut: 2000
              });
            }
            this.processService.removeTask(id);
          }));
        }
        else if (element.orderByRequestId) {
          let id: number = this.processService.addTask();
          this.subscriptions.push(this.orderService.deleteOrderByRequestByAdmin(element.orderByRequestId).subscribe(() => {
            this.ordersByRequest = this.ordersByRequest.filter(o => o.orderByRequestId != element.orderByRequestId);
            this.toastrService.success("Xóa Thành Công", "Thành Công", { timeOut: 2000 });
            this.processService.removeTask(id);
            this.countOrderByRequest();
          }, error => {
            if (error) {
              this.toastrService.error(error.error.Message, `Lỗi`, {
                timeOut: 2000
              });
            }
            else {
              this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
                timeOut: 2000
              });
            }
            this.processService.removeTask(id);
          }));
        }

      }
    });
  }

  update(element: any) {
    if (element.orderId) {
      if (element.status == 0) {
        this.subscriptions.push(this.orderService.updateOrderStatusByAdmin(element.orderId, OrderStatus.SEEN).subscribe(() => {
          this.formUpdate.controls.status.setValue(OrderStatus.SEEN);
        }));
      }
      this.isUpdate = !this.isUpdate;
      this.isOrder = true;
      this.formUpdate = this.fb.group({
        orderId: [element.orderId, Validators.required],
        customerName: [element.user.userName, Validators.required],
        email: [element.user.email],
        phoneNumber: [element.user.phoneNumber, Validators.required],
        address: [element.user.address, Validators.required],
        products: [element.products, Validators.required],
        orderDate: [element.orderDate, Validators.required],
        status: [element.status, Validators.required]
      });
    }
    else if (element.orderByRequestId) {
      if (element.status == 0) {
        this.subscriptions.push(this.orderService.updateOrderStatusByRequestByAdmin(element.orderByRequestId, OrderStatus.SEEN).subscribe(() => {
          this.formUpdate.controls.status.setValue(OrderStatus.SEEN);
        }));
      }
      this.isUpdate = !this.isUpdate;
      this.isOrder = false;
      this.formUpdate = this.fb.group({
        orderByRequestId: [element.orderByRequestId, Validators.required],
        customerName: [element.user.userName, Validators.required],
        email: [element.user.email],
        phoneNumber: [element.user.phoneNumber, Validators.required],
        address: [element.user.address, Validators.required],
        describeProduct: [element.describeProduct, Validators.required],
        orderDate: [element.orderDate, Validators.required],
        status: [element.status, Validators.required]
      });
    }
  }

  return() {
    this.isUpdate = !this.isUpdate;
    this.isOrder = true;
  }

  onUpdate() {
    console.log(this.formUpdate.value)
    if (this.isOrder) {
      let id: number = this.processService.addTask();
      let selectedOrder: Order;

      for (let i = 0; i < this.orders.length; i++) {
        if (this.orders[i].orderId == this.formUpdate.value['orderId']) {
          selectedOrder = this.orders[i];
          selectedOrder.products = this.formUpdate.value['products'];
          selectedOrder.status = this.formUpdate.value['status'];
          selectedOrder.user.userName = this.formUpdate.value['customerName'];
          selectedOrder.user.email = this.formUpdate.value['email'];
          selectedOrder.user.phoneNumber = this.formUpdate.value['phoneNumber'];
          selectedOrder.user.address = this.formUpdate.value['address'];
          break;
        }
      }
      this.subscriptions.push(this.orderService.updateOrderByAdmin(selectedOrder).subscribe(() => {
        for (let i = 0; i < this.orders.length; i++) {
          if (this.orders[i].orderId == selectedOrder.orderId) {
            this.orders[i] = <Order>selectedOrder;
            break;
          }
        }

        this.toastrService.success("Cập Nhật Thành Công", "Thành Công", { timeOut: 2000 });
        this.processService.removeTask(id);
        this.countOrder();
      }, error => {
        if (error) {
          this.toastrService.error(error.error.Message, `Lỗi`, {
            timeOut: 2000
          });
        }
        else {
          this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
            timeOut: 2000
          });
        }
        this.processService.removeTask(id);
      }));
    }
    else {
      let id: number = this.processService.addTask();
      let selectedOrder: OrderByRequest;

      for (let i = 0; i < this.ordersByRequest.length; i++) {
        if (this.ordersByRequest[i].orderByRequestId == this.formUpdate.value['orderByRequestId']) {
          selectedOrder = this.ordersByRequest[i];
          selectedOrder.describeProduct = this.formUpdate.value['describeProduct'];
          selectedOrder.status = this.formUpdate.value['status'];
          selectedOrder.user.userName = this.formUpdate.value['customerName'];
          selectedOrder.user.email = this.formUpdate.value['email'];
          selectedOrder.user.phoneNumber = this.formUpdate.value['phoneNumber'];
          selectedOrder.user.address = this.formUpdate.value['address'];
          break;
        }
      }

      this.subscriptions.push(this.orderService.updateOrderByRequestByAdmin(selectedOrder).subscribe(() => {
        for (let i = 0; i < this.ordersByRequest.length; i++) {
          if (this.ordersByRequest[i].orderByRequestId == selectedOrder.orderByRequestId) {
            this.ordersByRequest[i] = <OrderByRequest>selectedOrder
            break;
          }
        }
        this.toastrService.success("Cập Nhật Thành Công", "Thành Công", { timeOut: 2000 });
        this.processService.removeTask(id);
        this.countOrderByRequest();
      }, error => {
        if (error) {
          this.toastrService.error(error.error.Message, `Lỗi`, {
            timeOut: 2000
          });
        }
        else {
          this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
            timeOut: 2000
          });
        }
        this.processService.removeTask(id);
      }));
    }
  }

}
