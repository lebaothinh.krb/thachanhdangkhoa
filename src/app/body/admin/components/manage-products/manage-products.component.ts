import { Component, OnInit, OnDestroy } from '@angular/core';
import { Product, Topic } from 'src/app/shared/models/product';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ConstService } from 'src/app/services/const.service';
import { Subscription } from 'rxjs';
import { ProcessService } from 'src/app/services/process.service';
import { ProductService } from 'src/app/services/product.service';
import { ToastrService } from 'ngx-toastr';
import { UploadService } from 'src/app/services/upload.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { ProductTopic } from 'src/app/shared/models/productTopic';

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.css']
})
export class ManageProductsComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  subscriptions: Subscription[] = [];

  constructor(private fb: FormBuilder, private CONST: ConstService, private processService: ProcessService, private productService: ProductService, private toastrService: ToastrService, private uploadService: UploadService, private matDiaLog: MatDialog) { }

  formUpdate: FormGroup;
  isUpdate: boolean = false;
  isAdd: boolean = false;
  topics: Array<Topic>;

  selectedFile: File;
  selectedFileName: string = '';
  isFileSelected = false;

  displayedColumns = ['id', 'productName', 'material', 'size', 'imgs', 'topic', 'price', 'deleting', 'updating'];

  products: Array<Product> = []

  addContent() {
    this.isUpdate = !this.isUpdate;
    this.isAdd = true;
    this.createDefaultForm();
    this.onCheckTopic();
  }

  onCheckTopic() {
    let orArr: Array<Topic> = this.CONST.TOPIC_SEARCH.value;
    this.topics = Array.from(orArr);
    let filterArr: Array<ProductTopic> = this.formUpdate.controls.productTopics.value;
    for (let i = 1; i < orArr.length; i++) {
      for (let j = 0; j < filterArr.length; j++) {
        let cTopic = new Topic(orArr[i].topicId, orArr[i].topicName);
        if (cTopic.equalProductTopic(filterArr[j])) {
          this.topics = this.topics.filter(t => !new Topic(t.topicId, t.topicName).equal(orArr[i]))
          break;
        }
      }
    }
    this.topics.splice(0, 1);
  }

  getData() {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.productService.getProduct().subscribe((products: Array<Product>) => {
      this.products = products;
      console.log(this.products)
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  ngOnInit() {
    this.getData();
    this.createDefaultForm();
    this.onCheckTopic();
  }


  createDefaultForm() {
    this.formUpdate = this.fb.group({
      id: [-1, Validators.required],
      productName: ['', Validators.required],
      material: ['', Validators.required],
      size: ['', Validators.required],
      imgsToList: [['http://localhost:60699/Images/Products/default.jpg']],
      productTopics: [[], Validators.required],
      type: [this.CONST.TYPE_SEARCH.value[0], Validators.required],
      price: ['', Validators.required]
    });
  }

  removeImgs(img) {
    let newArr = (<Array<string>>this.formUpdate.value['imgsToList']).filter(i => i != img);
    if (newArr.length == 0) {
      this.formUpdate.controls.imgsToList.setValue(['http://localhost:60699/Images/Products/default.jpg']);
    }
    else {
      this.formUpdate.controls.imgsToList.setValue(newArr);
    }
  }

  removeTopic(topic) {
    let newArr = (<Array<Topic>>this.formUpdate.value['productTopics']).filter(i => i.topicId != topic.topicId);
    this.formUpdate.controls.productTopics.setValue(newArr);
    this.onCheckTopic();
  }

  delete(element) {
    this.matDiaLog.open(ConfirmDialogComponent, {
      data: "Bạn Có Muốn Xóa Sản Phẩm Này Không?"
    }).afterClosed().subscribe((result: boolean) => {
      if (result) {
        let id: number = this.processService.addTask();
        this.subscriptions.push(this.productService.deleteProductByAdmin(element.id).subscribe(() => {
          this.products = this.products.filter(p => p.id != element.id);
          this.toastrService.success("Xóa Thành Công", "Thành Công", { timeOut: 2000 });
          this.processService.removeTask(id);
        }, error => {
          if (error) {
            this.toastrService.error(error.error.Message, `Lỗi`, {
              timeOut: 2000
            });
          }
          else {
            this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
              timeOut: 2000
            });
          }
          this.processService.removeTask(id);
        }));
      }
    });
  }

  update(element) {
    this.isUpdate = true;
    this.isAdd = false;
    this.formUpdate = this.fb.group({
      id: [element.id, Validators.required],
      productName: [element.productName, Validators.required],
      material: [element.material, Validators.required],
      size: [element.size, Validators.required],
      imgsToList: [element.imgsToList],
      productTopics: [element.productTopics, Validators.required],
      type: [this.CONST.TYPE_SEARCH.value.filter(t => t.typeId == element.type.typeId)[0], Validators.required],
      price: [element.price, Validators.required]
    });
    this.onCheckTopic();
    console.log(this.formUpdate.value)
  }

  return() {
    this.isUpdate = false;
    this.isAdd = false;
  }

  onUpdate() {
    console.log(this.formUpdate.value)
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.productService.updateProductByAdmin(<Product>this.formUpdate.value).subscribe(() => {
      for (let i = 0; i < this.products.length; i++) {
        if (this.products[i].id == this.formUpdate.value['id']) {
          this.products[i] = <Product>this.formUpdate.value;
          break;
        }
      }
      this.toastrService.success("Cập Nhật Thành Công", "Thành Công", { timeOut: 2000 });
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];

    if (this.selectedFile) {
      let id = this.processService.addTask();
      let formData = new FormData();
      formData.set('file', this.selectedFile);
      formData.set('fileName', 'img' + Date.now().toString());

      this.subscriptions.push(this.uploadService.uploadImgProductByAdmin(formData).subscribe((data) => {
        let newArr = Array.from(this.formUpdate.value['imgsToList']);
        newArr.push(data.src);
        this.formUpdate.controls.imgsToList.setValue(newArr);
        this.processService.removeTask(id);
      }, error => {
        if (error) {
          this.toastrService.error(error.error.Message, `Lỗi`, {
            timeOut: 2000
          });
        }
        else {
          this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
            timeOut: 2000
          });
        }
        this.processService.removeTask(id);
      }));
      console.log(this.selectedFileName, this.selectedFile);
    }
  }

  onAdd() {
    console.log(this.formUpdate.value)
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.productService.createProductByAdmin(<Product>this.formUpdate.value).subscribe((id) => {
      this.formUpdate.controls.id.setValue(id);
      this.products.push(<Product>this.formUpdate.value);
      this.toastrService.success("Thêm Mới Thành Công", "Thành Công", { timeOut: 2000 });
      this.processService.removeTask(id);
      this.isAdd = !this.isAdd;
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  onChangeTopic(matSelectChange) {
    let selectedTopic: Topic = this.CONST.TOPIC_SEARCH.value.filter(t => t.topicId == matSelectChange.value)[0];
    let selectedProductTopic = new ProductTopic();
    selectedProductTopic.productId = this.formUpdate.value['id'];
    selectedProductTopic.topicId = selectedTopic.topicId;
    selectedProductTopic.topic = selectedTopic;

    let newArr: Array<ProductTopic>;
    if (selectedTopic) {
      newArr = Array.from(this.formUpdate.controls.productTopics.value);
      newArr.push(selectedProductTopic);
    }
    this.formUpdate.controls.productTopics.setValue(newArr);
    this.onCheckTopic();
    console.log(this.topics)
  }

}
