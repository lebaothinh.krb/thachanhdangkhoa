import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { UserService } from 'src/app/services/user.service';
import { MailForm } from 'src/app/shared/models/mailForm';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-send-info',
  templateUrl: './send-info.component.html',
  styleUrls: ['./send-info.component.css']
})
export class SendInfoComponent implements OnInit {

  constructor(private fb: FormBuilder, private matDiaLog: MatDialog, private userService: UserService, private toastrService: ToastrService) { }

  formInfo: FormGroup;

  ngOnInit() {
    this.formInfo = this.fb.group({
      subject: ['', Validators.required],
      content: ['', Validators.required],
    });
  }


  onChange(event) {
    this.formInfo.controls.content.setValue(event);
  }

  onUpdate(){
    console.log(this.formInfo)
    this.matDiaLog.open(ConfirmDialogComponent, {
      data: `Hãy kiểm tra nội dung thật kĩ trước khi gửi?
      Bạn có chắc chắn muốn gửi tin này tới khách hàng không`
    }).afterClosed().subscribe((result: boolean) => {
      if (result){
        this.userService.sendInfoByAdmin(<MailForm>this.formInfo.value).subscribe(() => {},
        error => {
          if (error) {
            this.toastrService.error(error.error.Message, `Lỗi`, {
              timeOut: 2000
            });
          }
          else {
            this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
              timeOut: 2000
            });
          }
        });
      }
    });
  }
}
