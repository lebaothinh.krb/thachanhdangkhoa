import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Content } from 'src/app/shared/models/content';
import { Topic } from 'src/app/shared/models/product';
import { ConstService } from 'src/app/services/const.service';
import { MatDialog } from '@angular/material';
import { LoginComponent } from 'src/app/body/login/login.component';
import { ToastrService } from 'ngx-toastr';
import { ProcessService } from 'src/app/services/process.service';
import { UploadService } from 'src/app/services/upload.service';
import { Subscription } from 'rxjs';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ContentService } from 'src/app/services/content.service';
import { TopicService } from 'src/app/services/topic.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-manage-home',
  templateUrl: './manage-home.component.html',
  styleUrls: ['./manage-home.component.css']
})
export class ManageHomeComponent implements OnInit, OnDestroy{
  
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  constructor(public sanitizer: DomSanitizer, private fb: FormBuilder,private CONST: ConstService, private topicService: TopicService,private contentService: ContentService, private toastrService: ToastrService, private processService: ProcessService, private uploadService: UploadService, private matDiaLog: MatDialog) { }

  formUpdate: FormGroup;
  isUpdate: boolean = false;
  isAdd: boolean = false;
  subscriptions: Subscription[] = [];

  displayedColumns = ['id', 'content', 'img', 'topic', 'deleting', 'updating'];
  contents: Array<Content> = [];

  selectedFile: File;
  selectedFileName: string;

  topics = [];

  addContent(){
    this.isUpdate = !this.isUpdate;
    this.isAdd = true;
    this.createEmptyForm();
  }
  
  createEmptyForm(){
    this.formUpdate = this.fb.group({
      id: [-1, Validators.required],
      sumaryContent: ['', Validators.required],
      img: ['http://localhost:60699/Images/Products/default.jpg', Validators.required],
      topic: [this.topics[0], Validators.required],
    });
  }

  ngOnInit() {
    this.topics = this.CONST.TOPIC_SEARCH.value;
    this.getData();
    this.createEmptyForm();
  }

  getData(){
    this.subscriptions.push(this.contentService.getHomeContents().subscribe((data: Array<Content>) => {
      this.contents = data;
    }));

    this.subscriptions.push(this.topicService.getTopics().subscribe((data: Array<Topic>) => {
      this.topics = data;
    }));
  }

  delete(element: Content) {
    this.matDiaLog.open(ConfirmDialogComponent, {
      data: "Bạn Có Muốn Xóa Nội Dung Này Không?"
    }).afterClosed().subscribe((result: boolean) => {
      if (result){
        let id: number = this.processService.addTask();
        this.subscriptions.push(this.contentService.deleteHomeContentByAdmin(element.id).subscribe(() => {
        this.contents = this.contents.filter(o => o.id != element.id);
          this.toastrService.success("Xóa Thành Công","Thành Công", {timeOut: 2000});
          this.processService.removeTask(id);
        }, error => {
          if (error) {
            this.toastrService.error(error.error.Message, `Lỗi`, {
              timeOut: 2000
            });
          }
          else {
            this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
              timeOut: 2000
            });
          }
          this.processService.removeTask(id);
        }));
      }
    });
  }

  onChangeTopic(matSelectChange){
    let selectedTopic:Topic = this.CONST.TOPIC_SEARCH.value.filter(t => t.topicId == matSelectChange.value)[0];
    this.formUpdate.controls.topic.setValue(selectedTopic);
  }

  update(element: Content) {
    this.isUpdate = true;
    this.isAdd = false;
    this.formUpdate = this.fb.group({
      id: [element.id, Validators.required],
      sumaryContent: [element.sumaryContent, Validators.required],
      img: [element.img, Validators.required],
      topic: [element.topic, Validators.required],
    });
  }

  removeImgs(img){
    this.formUpdate.controls.img.setValue('http://localhost:60699/Images/Products/default.jpg');
  }

  return() {
    this.isUpdate = false;
    this.isAdd = false;
  }

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
    this.selectedFileName = 'img' + Date.now().toString();

    if (this.selectedFile) {
      let id = this.processService.addTask();
      let formData = new FormData();
      formData.set('file', this.selectedFile);
      formData.set('fileName', this.selectedFileName);

      this.subscriptions.push(this.uploadService.uploadImgProductByAdmin(formData).subscribe((data) => {
        this.formUpdate.controls.img.setValue(data.src);
        this.processService.removeTask(id);
        console.log(data);
        return;
      }, error => {
        if (error) {
          console.log(error)
          this.toastrService.error(error.error.Message, `Lỗi`, {
            timeOut: 2000
          });
        }
        else {
          this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
            timeOut: 2000
          });
        }
        this.processService.removeTask(id);
      }));
    }
  }

  onUpdate() {
    console.log(this.formUpdate.value)
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.contentService.updateHomeContentByAdmin(<Content>this.formUpdate.value).subscribe(() => {
      for (let i = 0; i< this.contents.length; i++){
        if (this.contents[i].id == this.formUpdate.value['id']){
          this.contents[i] = <Content>this.formUpdate.value;
          break;
        }
      }

      this.toastrService.success("Cập Nhật Thành Công","Thành Công", {timeOut: 2000});
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  onAdd(){
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.contentService.createHomeContentByAdmin(<Content>this.formUpdate.value).subscribe((id) => {
      this.formUpdate.controls.id.setValue(id);
      this.contents.push(<Content>this.formUpdate.value);
      this.toastrService.success("Thêm Mới Thành Công","Thành Công", {timeOut: 2000});
      this.processService.removeTask(id);
      this.isAdd = !this.isAdd;
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }
}
