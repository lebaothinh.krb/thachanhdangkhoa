import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTopicTypeComponent } from './manage-topic-type.component';

describe('ManageTopicTypeComponent', () => {
  let component: ManageTopicTypeComponent;
  let fixture: ComponentFixture<ManageTopicTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageTopicTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTopicTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
