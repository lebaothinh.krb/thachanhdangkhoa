import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConstService } from 'src/app/services/const.service';
import { TopicService } from 'src/app/services/topic.service';
import { TypeService } from 'src/app/services/type.service';
import { Subscription } from 'rxjs';
import { Topic, Type } from 'src/app/shared/models/product';
import { ProcessService } from 'src/app/services/process.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-topic-type',
  templateUrl: './manage-topic-type.component.html',
  styleUrls: ['./manage-topic-type.component.css']
})
export class ManageTopicTypeComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  subscriptions: Subscription[] = [];
  topics: Array<Topic> = [];
  types: Array<Type> = [];

  constructor(private CONST: ConstService, private topicService: TopicService, private typeService: TypeService, private processService: ProcessService, private toastrService: ToastrService) { }

  ngOnInit() {
    this.getTopics();
    this.getTypes();
  }

  getTopics(){
    this.subscriptions.push(this.topicService.getTopics().subscribe((data: Array<Topic>) => {
      this.topics = data;
    }));
  }

  getTypes(){
    this.subscriptions.push(this.typeService.getTypes().subscribe((data: Array<Type>) => {
      this.types = data;
    }));
  }

  addTopic(topic){
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.topicService.addTopic(topic).subscribe((id) => {
      this.CONST.TOPIC_SEARCH.value.push(new Topic(id, topic));
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  addType(type){
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.typeService.addType(type).subscribe((id) => {
      this.CONST.TYPE_SEARCH.value.push(new Type(id, type));
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  deleteTopics(topics){
    console.log(topics)
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.topicService.deleteTopics(topics._value).subscribe(() => {
      topics._value.forEach(topic => {
        this.CONST.TOPIC_SEARCH.value = this.CONST.TOPIC_SEARCH.value.filter(t => t.topicId != topic);
      });
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  deleteTypes(types){
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.typeService.deleteTypes(types._value).subscribe(() => {
      types._value.forEach(type => {
        this.CONST.TYPE_SEARCH.value = this.CONST.TYPE_SEARCH.value.filter(t => t.typeId != type);
      });
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }
  
}
