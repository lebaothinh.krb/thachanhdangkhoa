import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConstService } from 'src/app/services/const.service';
import { News } from 'src/app/shared/models/news';
import { ProcessService } from 'src/app/services/process.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { NewsService } from 'src/app/services/news.service';
import { UploadService } from 'src/app/services/upload.service';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-manage-news',
  templateUrl: './manage-news.component.html',
  styleUrls: ['./manage-news.component.css']
})
export class ManageNewsComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  subscriptions: Subscription[] = [];

  constructor(private fb: FormBuilder, private CONST: ConstService, private processService: ProcessService, private toastrService: ToastrService, private newsService: NewsService, private uploadService: UploadService, private matDiaLog: MatDialog) { }

  formUpdate: FormGroup;
  isUpdate: boolean = false;
  isAdd: boolean = false;
  // topics: Array<Topic>;

  selectedFile: File;
  selectedFileName: string = '';
  isFileSelected = false;
  htmlContent: string = '';

  displayedColumns = ['id', 'title', 'sumaryContent', 'postedDate', 'poster', 'deleting', 'updating'];

  newes: Array<News> = [];

  ngOnInit() {
    this.createDefaultForm();
    this.getData();
  }

  getData(){
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.newsService.getNewes().subscribe((newes) => {
      this.newes = newes;
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  createDefaultForm() {
    this.formUpdate = this.fb.group({
      id: [-1, Validators.required],
      title: ['', Validators.required],
      htmlContent: ['', Validators.required],
      sumaryContent: ['', Validators.required],
      postedDate: [new Date(Date.now()), Validators.required],
      poster: ['', Validators.required],
    });
  }


  delete(element: News) {
    this.matDiaLog.open(ConfirmDialogComponent, {
      data: "Bạn Có Muốn Xóa Tin Này Không?"
    }).afterClosed().subscribe((result: boolean) => {
      if (result){
        let id: number = this.processService.addTask();
        this.subscriptions.push(this.newsService.deleteNewsByAdmin(element.id).subscribe(() => {
        this.newes = this.newes.filter(n => n.id != element.id);
          this.toastrService.success("Xóa Thành Công","Thành Công", {timeOut: 2000});
          this.processService.removeTask(id);
        }, error => {
          if (error) {
            this.toastrService.error(error.error.Message, `Lỗi`, {
              timeOut: 2000
            });
          }
          else {
            this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
              timeOut: 2000
            });
          }
          this.processService.removeTask(id);
        }));
      }
    });
  }

  update(element: News) {
    this.isUpdate = true;
    this.isAdd = false;
    this.formUpdate = this.fb.group({
      id: [element.id, Validators.required],
      title: [element.title, Validators.required],
      htmlContent: [element.htmlContent, Validators.required],
      sumaryContent: [element.sumaryContent, Validators.required],
      postedDate: [element.postedDate, Validators.required],
      poster: [element.poster, Validators.required],
    });
  }

  onChange(event) {
    this.formUpdate.controls.htmlContent.setValue(event);
  }
  return() {
    this.isUpdate = false;
    this.isAdd = false;
  }

  onUpdate(event) {
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.newsService.updateNewsByAdmin(<News>this.formUpdate.value).subscribe(() => {
      for (let i = 0 ; i< this.newes.length; i++){
        if (this.newes[i].id == this.formUpdate.value['id']){
          this.newes[i] = <News>this.formUpdate.value;
        }
      }
      this.toastrService.success("Cập Nhật Thành Công","Thành Công", {timeOut: 2000});
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];

    if (this.selectedFile) {
      let id = this.processService.addTask();

      let formData = new FormData();
      formData.set('file', this.selectedFile);
      formData.set('fileName', 'img' + Date.now().toString());

      this.subscriptions.push(this.uploadService.uploadImgNewsByAdmin(formData).subscribe((data) => {
        this.formUpdate.controls.poster.setValue(data.src);
        this.processService.removeTask(id);
      }, error => {
        if (error) {
          this.toastrService.error(error.error.Message, `Lỗi`, {
            timeOut: 2000
          });
        }
        else {
          this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
            timeOut: 2000
          });
        }
        this.processService.removeTask(id);
      }));
    }
  }

  addContent(){
    this.isUpdate = !this.isUpdate;
    this.isAdd = true;
    this.createDefaultForm();
  }

  onAdd(){
    console.log(this.formUpdate.value)
    this.formUpdate.controls.postedDate.setValue(new Date(Date.now()));
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.newsService.createNewsByAdmin(<News>this.formUpdate.value).subscribe((id) => {
      this.formUpdate.controls.id.setValue(id);
      this.newes.push(<News>this.formUpdate.value);
      this.toastrService.success("Thêm Mới Thành Công","Thành Công", {timeOut: 2000});
      this.processService.removeTask(id);
      this.isAdd = !this.isAdd;
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

}
