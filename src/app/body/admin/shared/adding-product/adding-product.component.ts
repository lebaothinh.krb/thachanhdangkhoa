import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ProcessService } from 'src/app/services/process.service';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/shared/models/product';
import { Subscription } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-adding-product',
  templateUrl: './adding-product.component.html',
  styleUrls: ['./adding-product.component.css']
})
export class AddingProductComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  constructor(@Inject(MAT_DIALOG_DATA) public removeProducts: Array<Product>, private processService: ProcessService, private toastrService: ToastrService, private productService: ProductService, private dialogRef: MatDialogRef<AddingProductComponent>,) { }

  products: Array<Product> = [];
  subscriptions: Subscription[] = [];
  selectedProduct: Product = null;

  ngOnInit() {
    this.getData();
  }

  onClose(){
    this.dialogRef.close();
  }
  
  getData(){
    let id: number = this.processService.addTask();
    this.subscriptions.push(this.productService.getProduct().subscribe((products: Array<Product>) => {
      this.products = products.filter(t => this.removeProducts.filter(rp => rp.id != t.id).length);
      this.processService.removeTask(id);
    }, error => {
      if (error) {
        this.toastrService.error(error.error.Message, `Lỗi`, {
          timeOut: 2000
        });
      }
      else {
        this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
          timeOut: 2000
        });
      }
      this.processService.removeTask(id);
    }));
  }

  select(event, product: Product){
    let els = document.getElementsByClassName('product-card');

    for (let i=0; i< els.length ; i++){
      if (els[i].contains(<Node>event.target))
      {
        els[i].className = 'product-card active';
      }
      else els[i].className = 'product-card';
    }
    this.selectedProduct = product;
  }

}
