import { Component, OnInit, ViewChild } from '@angular/core';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';

@Component({
  selector: 'app-crop-image',
  templateUrl: './crop-image.component.html',
  styleUrls: ['./crop-image.component.css']
})
export class CropImageComponent implements OnInit {

  @ViewChild(ImageCropperComponent, {static: true}) imageCropper: ImageCropperComponent;
  
  constructor() { }


  ngOnInit() {
    
  }

  imageChangedEvent: any = '';
  croppedImage: any = '';

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log(this.croppedImage);
  }
  imageLoaded() {
    // show cropper
    console.log("imageLoaded");
  }
  cropperReady() {
    // cropper ready
    console.log("cropperReady");
  }
  loadImageFailed() {
    // show message
    console.log("loadImageFailed");
  }

}
