import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BgIntroComponent } from './shared/components/bg-intro/bg-intro.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatIconRegistry, MatChipsModule, MatTooltipModule, MatStepperModule, MatProgressBarModule } from '@angular/material';

import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './body/home/home.component';
import { AppRoutingModule } from './app.routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ProductsComponent } from './body/products/products.component';
import { ProductCardComponent } from './shared/components/product-card/product-card.component';
import { OrderComponent } from './body/order/order.component';
import { NewsComponent } from './body/news/news.component';
import { LoginComponent } from './body/login/login.component';
import { AboutUsComponent } from './body/about-us/about-us.component';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { NewsCardComponent } from './shared/components/news-card/news-card.component';
import { FbCommentsComponent } from './shared/components/fb-comments/fb-comments.component';
import { NewsDetailComponent } from './body/news/news-detail/news-detail.component';
import { OrderByRequestComponent } from './body/order/order-by-request/order-by-request.component';
import { ToastrModule } from 'ngx-toastr';
import { ProductCardDetailComponent } from './shared/components/product-card-detail/product-card-detail.component';
import { SharedModule } from './shared/modules/shared/shared.module';
import { JwtModule } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

export function tokenGetter() {
  return localStorage.getItem("jwt");
}

@NgModule({
  declarations: [
    AppComponent,
    BgIntroComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    HomeComponent,
    ProductsComponent,
    ProductCardComponent,
    ProductCardDetailComponent,
    LoginComponent,
    OrderComponent,
    NewsComponent,
    AboutUsComponent,
    NewsCardComponent,
    NewsDetailComponent,
    FbCommentsComponent,
    OrderByRequestComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatChipsModule,
    MatTooltipModule,
    MatStepperModule,
    MatProgressBarModule,
    SharedModule,
    InfiniteScrollModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ["http://localhost:60699"],
        blacklistedRoutes: []
      }
    }),
    ToastrModule.forRoot(),
  ],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false }
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ProductCardDetailComponent,
    LoginComponent,
    OrderComponent,
    OrderByRequestComponent
  ]
})
export class AppModule { 
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')); // Or whatever path you placed mdi.svg at
  }
}
