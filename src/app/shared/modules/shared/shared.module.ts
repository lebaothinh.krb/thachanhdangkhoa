import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatSelectModule, MatFormFieldModule, MatInputModule, MatIconModule, MatOptionModule, MatDialogModule, MatBadgeModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { WordEditorComponent } from '../../components/word-editor/word-editor.component';
import { PageNotFoundComponent } from '../../components/page-not-found/page-not-found.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ConstService } from 'src/app/services/const.service';
import { ProductService } from 'src/app/services/product.service';
import { ProcessService } from 'src/app/services/process.service';
import { UploadService } from 'src/app/services/upload.service';
import { NewsService } from 'src/app/services/news.service';
import { UserService } from 'src/app/services/user.service';
import { OrderService } from 'src/app/services/order.service';
import { AddingProductComponent } from 'src/app/body/admin/shared/adding-product/adding-product.component';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';
import { TopicService } from 'src/app/services/topic.service';
import { TypeService } from 'src/app/services/type.service';
import { ContentService } from 'src/app/services/content.service';
import { AuthGuard } from 'src/app/services/auth-guard.service';
import { CropImageComponent } from 'src/app/body/admin/shared/crop-image/crop-image.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SafeHtmlPipe } from '../../pipes/safeHtmlPipe';


@NgModule({
  declarations: [
    WordEditorComponent,
    PageNotFoundComponent,
    AddingProductComponent, 
    ConfirmDialogComponent,
    CropImageComponent,
    SafeHtmlPipe
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatSelectModule,
    MatOptionModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    EditorModule,
    FormsModule,
    MatDialogModule,
    MatBadgeModule,
    ImageCropperModule
  ],
  providers: [
    ConstService,
    ProductService,
    ProcessService,
    NewsService,
    UploadService,
    UserService,
    OrderService,
    TopicService,
    TypeService,
    ContentService,
    AuthGuard
  ],
  exports: [
    WordEditorComponent,
    PageNotFoundComponent,
    AddingProductComponent,
    ConfirmDialogComponent,
    CommonModule,
    MatButtonModule,
    MatSelectModule,
    MatOptionModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    EditorModule,
    FormsModule,
    MatBadgeModule,
    SafeHtmlPipe
  ],
  entryComponents: [
    AddingProductComponent,
    ConfirmDialogComponent,
    CropImageComponent
  ]
})
export class SharedModule { }
