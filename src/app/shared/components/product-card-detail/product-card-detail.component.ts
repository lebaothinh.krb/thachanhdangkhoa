import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Product } from '../../models/product';
import { ConstService } from 'src/app/services/const.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-card-detail',
  templateUrl: './product-card-detail.component.html',
  styleUrls: ['./product-card-detail.component.css']
})
export class ProductCardDetailComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ProductCardDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public product: Product, private CONST: ConstService, private router: Router) { }

  selectedImg: string = '';

  ngOnInit() {
    this.selectedImg = this.product.imgsToList[0];
  }

  closeDialog() {
    this.dialogRef.close();
  }

  active(event: MouseEvent, selectedImg){
    let els = document.getElementsByClassName('small-img-area-detail');

    for (let i=0; i< els.length ; i++){
      if (els[i].contains(<Node>event.target))
      {
        els[i].className = 'small-img-area-detail active';
      }
      else els[i].className = 'small-img-area-detail';
    }
    this.selectedImg = selectedImg;
  }

  getProduct(topic: string){
    this.closeDialog();
    this.router.navigate(
      [
        '/products',
        {
          searchingKey: '',
          topic: topic,
          type: 1
        }
      ]
    );
  }
}
