import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ViewChild, AfterViewInit } from '@angular/core';
import { defaultConfig, basicConfig } from './config/word-editor.config';
import { ConstService } from 'src/app/services/const.service';

@Component({
  selector: 'app-word-editor',
  templateUrl: './word-editor.component.html',
  styleUrls: ['./word-editor.component.css']
})
export class WordEditorComponent implements OnInit, OnChanges {

  constructor(private CONST: ConstService) { }

  @Input() data: string = '';
  @Input() isDefaultConfig = true;
  @ViewChild('editor', { static: true}) editor: any;

  @Output('onChangeInput') onChangeInput = new EventEmitter<string>();

  get getData() {
    return this.data;
  }

  set setData(data) {
    this.data = data;
  }

  config: any = basicConfig;

  apiKey = this.CONST.API_KEY;

  ngOnInit() {
    
  }
  
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.getConfig();
  }

  getConfig() {
    if (this.isDefaultConfig) {
      this.config = defaultConfig;
    }
    else {
      this.config = basicConfig;
    }
  }

  onChange() {
    this.onChangeInput.emit(this.getData);
  }
}
