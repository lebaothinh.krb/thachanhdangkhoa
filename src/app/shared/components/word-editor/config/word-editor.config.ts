const VALID_ELEMENTS = '*[id|class|title|align|role|colspan|style|data-options|highlight|src|height|width|href|icopocidx|dir|linkProtocol|maplink|sas-link|generate-sas-link]'
const VALID_DIFF_ELEMENTS = '*[id|class|title|align|role|colspan|style|data-options|src|height|width|href|icopocidx|dir|linkProtocol|maplink|sas-link|generate-sas-link]'
const uploadConfig: object = {
  images_upload_url: 'postAcceptor.php',
  images_upload_handler: function (blobInfo, success, failure) {
    var xhr, formData;
    xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.open('POST', 'postAcceptor.php');
    xhr.onload = function () {
      var json;

      if (xhr.status != 200) {
        failure('HTTP Error: ' + xhr.status);
        return;
      }
      json = JSON.parse(xhr.responseText);

      if (!json || typeof json.location != 'string') {
        failure('Invalid JSON: ' + xhr.responseText);
        return;
      }
      success(json.location);
    };
    formData = new FormData();
    formData.append('file', blobInfo.blob(), blobInfo.filename());
    formData.append('fileName', 'img' + Date.now().toString());

    let url = 'http://localhost:60699/api/Uploads/updateImgEditor';
    xhr.open('POST', url, true);
    xhr.onload = function () {
      // do something to response
      let data: any = JSON.parse(this.response);
      console.log(this.response)
      success(data.src);
    };
    xhr.send(formData);
  }
}

const baseConfig: object = {
  branding: false,
  // tslint:disable-next-line: max-line-length
  plugins: 'fullpage media template charmap hr pagebreak  toc  wordcount  noneditable table image code',
  // tslint:disable-next-line: max-line-length
  toolbar: 'formatnews image formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat save',
  entity_encoding: 'raw',
  visual: false,
  imageUrl: getPathHost(),
  content_css: `${getPathHost()}/assets/tiny.css`,
  noneditable_noneditable_class: "mceNonEditable",
  setup: function (editor) {
    var toDateHtml = function (date) {
      return '<time datetime="' + date.toString() + '">' + date.toDateString() + '</time>';
    };

    editor.ui.registry.addButton('formatnews', {
      icon: 'template',
      tooltip: 'Format the document',
      onAction: function (_) {
        console.log(editor.dom.doc.body);
        let body: HTMLElement = editor.dom.doc.body;
        let imgs = editor.dom.select('img');

        editor.dom.setStyle(imgs, 'display', 'block');
        editor.dom.setStyle(imgs, 'margin-left', 'auto');
        editor.dom.setStyle(imgs, 'margin-right', 'auto');
        editor.dom.setStyle(imgs, 'max-width', '100%');
        editor.dom.setStyle(imgs, 'height', '');
        editor.dom.setStyle(imgs, 'width', '650px');
        
        editor.dom.setHTML();
      },
      onSetup: function (buttonApi) {
        var editorEventCallback = function (eventApi) {
          buttonApi.setDisabled(eventApi.element.nodeName.toLowerCase() === 'time');
        };
        editor.on('NodeChange', editorEventCallback);
        return function (buttonApi) {
          editor.off('NodeChange', editorEventCallback);
        }
      }
    });

  }
}

function getPathHost() {
  let port = window.location.port == '' ? '' : `:${window.location.port}`
  return `${window.location.protocol}//${window.location.hostname}${port}`

}

function getBasicConfig() {
  const prop =
  {
    menubar: false,
    statusbar: false,
    toolbar: 'image',
    plugins: 'image',
    width: '100%',
    // readonly : 1,
    // height: '100%',
    // valid_elements: VALID_ELEMENTS
  }
  return Object.assign(prop, uploadConfig)
}

function getDefaultConfig() {
  const prop =
  {
    // menubar: false,
    // statusbar: false,
    // toolbar: false,
    // readonly : 1,
    height: '90vh',
    valid_elements: VALID_ELEMENTS
  }
  return Object.assign(prop, baseConfig, uploadConfig)
}

export let defaultConfig: object = getDefaultConfig()
export let basicConfig: object = getBasicConfig();