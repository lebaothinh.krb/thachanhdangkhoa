import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../models/product';
import { MatDialog } from '@angular/material';
import { ConstService } from 'src/app/services/const.service';
import { ProductCardDetailComponent } from '../product-card-detail/product-card-detail.component';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {

  @Input('data') product: Product;
  constructor(private CONST: ConstService, private matDiaLog: MatDialog) { }

  ngOnInit() {
  }

  openDetailDialog(){
    this.matDiaLog.open(ProductCardDetailComponent,{
      data: this.product,
    });
  }

  addToCart(){
    this.CONST.addToCart(this.product);
  }

}
