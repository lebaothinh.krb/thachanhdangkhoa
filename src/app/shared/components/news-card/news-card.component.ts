import { Component, OnInit, Input } from '@angular/core';
import { News } from '../../models/news';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.css']
})
export class NewsCardComponent implements OnInit {

  constructor(private _sanitizer: DomSanitizer) { }

  @Input() news: News;

  get newsImg(){
    return this._sanitizer.bypassSecurityTrustStyle(`url(${this.news.poster.split('\\').join('/')})`);
  }

  ngOnInit() {
  }

}
