import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fb-comments',
  templateUrl: './fb-comments.component.html',
  styleUrls: ['./fb-comments.component.css']
})
export class FbCommentsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const fb = document.getElementById('fb');
    fb.setAttribute('data-href', window.location.href);
  }

}
