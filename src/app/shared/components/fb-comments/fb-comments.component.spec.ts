import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbCommentsComponent } from './fb-comments.component';

describe('FbCommentsComponent', () => {
  let component: FbCommentsComponent;
  let fixture: ComponentFixture<FbCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
