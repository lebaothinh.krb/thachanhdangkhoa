import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Topic } from '../../models/product';

@Component({
  selector: 'app-bg-intro',
  templateUrl: './bg-intro.component.html',
  styleUrls: ['./bg-intro.component.css'],
})
export class BgIntroComponent implements OnInit {

  constructor(private router: Router) { }
  @Input() isLeft: boolean;
  @Input() content: any;

  ngOnInit() {
  }

  getProduct(topic: Topic){
    this.router.navigate(
      [
        '/products',
        {
          searchingKey: '',
          topic: topic.topicId,
          type: 1
        }
      ]
    );
  }

}
