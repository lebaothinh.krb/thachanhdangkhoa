import { Product } from './product';
import { User } from './user';

export enum OrderStatus {
    NOTSEEN,
    SEEN,
    HANDLING,
    FINISH
}

export class Order{
    public orderId: number = -1;
    public user: User;
    public products: Array<Product>;
    public orderDate: Date = new Date(Date.now());
    public status: OrderStatus = OrderStatus.NOTSEEN;
    
    constructor (user: User, products: Array<Product>){
        this.user = user;
        this.products = products;
    }
}

export class OrderByRequest{
    public orderByRequestId: number = -1;
    public user: User;
    public describeProduct: string;
    public orderDate: Date = new Date(Date.now());
    public status: OrderStatus = OrderStatus.NOTSEEN;
    
    constructor (user: User, describeProduct: string){
        this.user = user
        this.describeProduct = describeProduct;
    }
}