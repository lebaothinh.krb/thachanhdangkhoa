import { Topic } from './product';

export class ProductTopic{
    public productId: number;
    public topicId: number;
    public topic: Topic;
}