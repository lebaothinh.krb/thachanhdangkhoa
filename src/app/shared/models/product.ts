import { ProductTopic } from './productTopic';

export class Topic{
    public topicName: string;
    public topicId: number;
    public productTopics: ProductTopic;

    constructor(value, displayValue) {
        this.topicName = displayValue;
        this.topicId = value;
    }

    public equal (topic: Topic): boolean{
        if (this.topicName == topic.topicName && this.topicId == topic.topicId) return true;
        return false;
    }

    public equalProductTopic (topic: ProductTopic): boolean{
        if (this.topicName == topic.topic.topicName && this.topicId == topic.topicId) return true;
        return false;
    }

    public toString(): string{
        return this.topicName;
    }
}

export class Type{
    public typeName: string;
    public typeId: number;

    constructor(value, displayValue) {
        this.typeName = displayValue;
        this.typeId = value;
    }

    public equal (topic: Type): boolean{
        if (this.typeName == topic.typeName && this.typeId == topic.typeId) return true;
        return false;
    }

    public toString(): string{
        return this.typeName;
    }
}


export class Product {
    public id: number;
    public productName: string;
    public material: string;
    public size: string;
    public imgsToList: Array<string>;
    public productTopics: Array<ProductTopic>;
    public price: string;
    public type: Type;

    constructor(id: number, productName: string, material: string, size: string, imgs: Array<string>, topic: Array<ProductTopic>, price: string, type: Type){
        this.id = id;
        this.productName = productName;
        this.material = material;
        this.size = size;
        this.imgsToList = imgs;
        this.productTopics = topic;
        this.price = price;
        this.type = type;
    }

    public toString(): string{
        return this.productName;
    }
}