export class MailForm{
    public subject: string;
    public content: string;

    constructor (subject: string, content: string){
        this.subject = subject;
        this.content = content;        
    }
}