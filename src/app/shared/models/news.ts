export class News {
    public id: number =-1;
    public title: string;
    public htmlContent: string;
    public sumaryContent: string;
    public postedDate: Date = new Date(Date.now());
    public poster: string;
    
    constructor (title, sumaryContent, poster, htmlContent = ''){
        this.title = title;
        this.sumaryContent = sumaryContent;
        this.poster = poster;
        this.htmlContent = htmlContent;
        this.postedDate = new Date(Date.now());
    }
}