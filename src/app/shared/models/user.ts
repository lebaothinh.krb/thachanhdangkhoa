export class User {
    public userId: number;
    public userName: string;
    public email: string;
    public address: string;
    public phoneNumber: string
    public account: string
    public password: string
    public role: Object
}