import { Topic } from './product';

export class Content{
    public id: number = -1;
    public sumaryContent: string;
    public img: string;
    public topic: Topic;

    constructor (content: string, img: string, topic: Topic){
        this.sumaryContent = content;
        this.img = img;
        this.topic = topic;
    }
}