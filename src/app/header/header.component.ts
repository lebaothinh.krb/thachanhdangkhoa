import { Component, OnInit } from '@angular/core';
import { ConstService } from '../services/const.service';
import { MatDialog } from '@angular/material';
import { LoginComponent } from '../body/login/login.component';
import { OrderComponent } from '../body/order/order.component';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ProcessService } from '../services/process.service';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private jwtHelper: JwtHelperService, private router: Router, private CONST: ConstService, private matDiaLog: MatDialog, private formBuilder: FormBuilder, private processService: ProcessService) { }

  searchingForm: FormGroup;

  isUserAuthenticated() {
    let token: string = localStorage.getItem("jwt");
    if (token && !this.jwtHelper.isTokenExpired(token)) {
      return true;
    }
    else {
      return false;
    }
  }

  ngOnInit() {
    this.searchingForm = this.formBuilder.group({
      searchingKey: ['', Validators.required],
      topic: [1, Validators.required],
      type: [1, Validators.required],
    })
  }

  onOpenLogin(btn) {
    this.closeMenu(btn);
    this.matDiaLog.open(LoginComponent, {});
  }

  onLogOut(){
    localStorage.removeItem("jwt");
    this.router.navigate(["/home"]);
  }

  closeMenu(btn) {
    if (Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 576)
      btn.click();
  }

  openCart() {
    this.matDiaLog.open(OrderComponent, {});
  }

  onSearch() {
    this.router.navigate(
      [
        '/products',
        this.searchingForm.value
      ]
    );
  }
}
