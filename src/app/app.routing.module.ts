import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './body/home/home.component';
import { ProductsComponent } from './body/products/products.component';
import { AboutUsComponent } from './body/about-us/about-us.component';
import { NewsComponent } from './body/news/news.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { NewsDetailComponent } from './body/news/news-detail/news-detail.component';
import { AuthGuard } from './services/auth-guard.service';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    { path: 'home', component: HomeComponent },
    { path: 'products', component: ProductsComponent },
    { path: 'aboutus', component: AboutUsComponent },
    { path: 'news', component: NewsComponent },
    { path: 'news/:id', component: NewsDetailComponent},
    {
        path: 'admin',
        loadChildren: () => import('../app/body/admin/admin.module').then(m => m.AdminModule),
        canActivate: [AuthGuard]
    },
    { path: '**', component: PageNotFoundComponent},
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, {scrollPositionRestoration: 'enabled'}),
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
