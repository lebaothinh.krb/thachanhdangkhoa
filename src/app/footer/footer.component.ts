import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConstService } from '../services/const.service';
import { MatDialog } from '@angular/material';
import { OrderByRequestComponent } from '../body/order/order-by-request/order-by-request.component';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../services/user.service';
import { ProcessService } from '../services/process.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  subscriptions: Subscription[] = [];
  
  constructor(private CONST: ConstService, private matDialog: MatDialog, private processService: ProcessService, private toastrService: ToastrService, private userService: UserService) { }

  ngOnInit() {

  }

  openOrderByRequestDialog() {
    this.matDialog.open(OrderByRequestComponent, {
      width: '100vw',
      maxHeight: '90vh'
    });
  }

  validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return (true)
    }
    return (false)
  }

  registerEmail(email) {
    if (this.validateEmail(email)) {
      let id: number = this.processService.addTask();
      this.subscriptions.push(this.userService.registerEmail(email).subscribe(() => {
        this.toastrService.success(email, "Thành Công", {
          timeOut: 2000
        });
        this.processService.removeTask(id);
      }, error => {
        if (error) {
          this.toastrService.error(error.error.Message, `Lỗi`, {
            timeOut: 2000
          });
        }
        else {
          this.toastrService.error(`Lỗi không xác định`, `Lỗi`, {
            timeOut: 2000
          });
        }
        this.processService.removeTask(id);
      }));
    }
    else {
      this.toastrService.error("Vui lòng nhập email nhập hợp lệ!", "Thất Bại", {
        timeOut: 2000
      })
    }
  }

  goToSocialMedia(url) {
    window.location.href = url;
  }
}
