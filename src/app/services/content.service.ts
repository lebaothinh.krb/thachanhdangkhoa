import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MailForm } from '../shared/models/mailForm';
import { Content } from '../shared/models/content';
import { JwtHelperService } from '@auth0/angular-jwt';
// import { environment } from '../../environments/environment';
@Injectable()
export class ContentService {

    public API_URL = environment.API_URL+'/contents/';

    constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {
    }

    public getHomeContents(): Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.get(`${this.API_URL}getHomeContents`, {headers: header});
    }

    public updateHomeContentByAdmin(content: Content): Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.post(`${this.API_URL}updateHomeContentByAdmin`, content, { headers: header })
    }

    public deleteHomeContentByAdmin(id: number): Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.get(`${this.API_URL}deleteHomeContentByAdmin/${id}`, { headers: header })
    }

    public createHomeContentByAdmin(content: Content): Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.post(`${this.API_URL}createHomeContentByAdmin`, content, { headers: header })
    }
}