import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MailForm } from '../shared/models/mailForm';
// import { environment } from '../../environments/environment';
@Injectable()
export class TopicService {

    public API_URL = environment.API_URL+'/topics/';

    constructor(private http: HttpClient) {
    }

    public getTopics(): Observable<Object> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.get(`${this.API_URL}getTopics`, {headers: header});
    }

    public deleteTopics(listValue: Array<string>):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL}deleteTopics`,listValue, {headers: header});
    }

    public addTopic(name: string):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.get(`${this.API_URL}addTopic/${name}`, {headers: header});
    }
}