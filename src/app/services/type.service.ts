import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MailForm } from '../shared/models/mailForm';
// import { environment } from '../../environments/environment';
@Injectable()
export class TypeService {

    public API_URL = environment.API_URL+'/types/';

    constructor(private http: HttpClient) {
    }

    public getTypes(): Observable<Object> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.get(`${this.API_URL}getTypes`, {headers: header});
    }

    public deleteTypes(listValue: Array<string>):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL}deleteTypes`,listValue, {headers: header});
    }

    public addType(name: string):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.get(`${this.API_URL}addType/${name}`, {headers: header});
    }
}