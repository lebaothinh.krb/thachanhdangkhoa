import { Observable, Subscription } from 'rxjs';
import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product, Topic, Type } from '../shared/models/product';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { Content } from '../shared/models/content';
import { ContentService } from './content.service';
import { TopicService } from './topic.service';
import { TypeService } from './type.service';
@Injectable()
export class ConstService implements OnDestroy {

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    private subscriptions: Subscription[] = [];

    public API_URL = environment.API_URL + '/Const/';

    // home info
    HOME_CONTENTS: Array<Content> = [];

    // api Tiny key
    API_KEY = 'dh448fao28ctmlanzex5aul7x6ga7o2ko5ozkueobq1po7lf';

    // CART
    private CART: Array<Product> = [];

    public clearCart() {
        this.CART = [];
        localStorage.removeItem('cart');
    }

    get getCart() {
        return this.CART;
    }

    public removeProductFromCart(id: number) {
        this.CART = this.CART.filter(p => p.id != id);
        localStorage.setItem('cart', JSON.stringify(this.CART))
    }

    public addToCart(product: Product) {
        product.productTopics = null;
        product.type = null;
        if (this.CART.filter(c => c.id == product.id).length > 0) {
            this.toastrService.warning(`Sản phẩm đã tồn tại trong giỏ hàng. Nếu muốn lấy số lượng nhiều vui lòng chọn 'ĐẶT HÀNG THEO YÊU CẦU' bên dưới`, "Cảnh Báo", {
                timeOut: 5000
            })
            return;
        }
        this.CART.push(product);
        this.toastrService.success(`Đã thêm sản phẩm vào giỏ hàng`, "Thành Công", {
            timeOut: 2000
        })
        localStorage.setItem('cart', JSON.stringify(this.CART))
    }

    // SEARCHING INFORMATION
    TOPIC_SEARCH = {
        name: 'Chủ Đề',
        value: [
            new Topic(1, "Tất Cả")
        ],
        color: '#00B0E8',
    };

    TYPE_SEARCH = {
        name: 'Thể Loại',
        value: [
            new Type(1, "Tất Cả")
        ],
        color: '#E89300'
    };

    // HEADER INFORMATION
    HEADERS = [
        {
            name: 'TRANG CHỦ',
            href: '/home'
        },
        {
            name: 'SẢN PHẨM',
            href: '/products'
        },
        {
            name: 'GIỚI THIỆU',
            href: '/aboutus'
        },
        {
            name: 'TIN TỨC',
            href: '/news'
        }
    ]

    // INFORMATION OF WEBSITE;
    SITE_NAME = 'THẠCH ẢNH ĐĂNG KHOA';
    ADDRESS = '9/1 Đường Nguyễn Thị Định, Khối 4, Thị Trấn Krông Kmar, Huyện Krông Bông, Tỉnh Daklak';
    EMAIL = 'thachanhdangkhoa@gmail.com';
    CONTACT = '0354141413 - 0347013955 - Mr.Khoa';

    BRANCHES = [
        '9/1 Đường Nguyễn Thị Định, Khối 4, Thị Trấn Krông Kmar, Huyện Krông Bông, Tỉnh Daklak',
        '9/1 Đường Nguyễn Thị Định, Khối 4, Thị Trấn Krông Kmar, Huyện Krông Bông, Tỉnh Daklak',
        '9/1 Đường Nguyễn Thị Định, Khối 4, Thị Trấn Krông Kmar, Huyện Krông Bông, Tỉnh Daklak'
    ];

    FACEBOOK_ADDRESS = 'https://www.facebook.com/ledangkhoa1983';
    ZALO_ADDRESS = '';

    private getOldCart() {
        let cart = localStorage.getItem('cart');
        if (cart) {
            this.CART = JSON.parse(cart);
            console.log("get Old Cart success");
        }
    }

    private getHomeContents() {
        this.subscriptions.push(this.contentService.getHomeContents().subscribe((data: Array<Content>) => {
            this.HOME_CONTENTS = data;
            console.log("get Home Content success", this.HOME_CONTENTS);
        }, error => {
            console.log("get Home Content fail", this.HOME_CONTENTS);
        }));
    }

    private getTypeSearch() {
        this.subscriptions.push(this.typeService.getTypes().subscribe((data: Array<Type>) => {
            this.TYPE_SEARCH.value = data;
            console.log("get Type Search success", this.TYPE_SEARCH);
        }, error => {
            console.log("get Type Search fail", this.TYPE_SEARCH);
        }));
    }

    private getTopicSearch() {
        this.subscriptions.push(this.topicService.getTopics().subscribe((data: Array<Topic>) => {
            this.TOPIC_SEARCH.value = data;
            console.log("get Topic Search success", this.TOPIC_SEARCH);
        }, error => {
            console.log("get Topic Search fail", this.TOPIC_SEARCH);
        }));
    }

    constructor(private toastrService: ToastrService, private contentService: ContentService, private topicService: TopicService, private typeService: TypeService) {
        this.getOldCart();
        this.getHomeContents();
        this.getTypeSearch();
        this.getTopicSearch();
    }
    // public getFeatureContestDTO(): Observable<any> {
    //     let header = new HttpHeaders();
    //     header = header.append('Authorization', localStorage.getItem('token'));
    //     return this.http.get(`${this.API_URL}contest/feature`,{ headers: header });
    // }
}
