import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { News } from '../shared/models/news';
// import { environment } from '../../environments/environment';
@Injectable()
export class NewsService {

    public API_URL = environment.API_URL+'/newes/';

    constructor(private http: HttpClient) {
    }

    public getAllNewes(index):Observable<any>{
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.get(`${this.API_URL}getAllNewes/${index}`, {headers: header});
    }

    public getSumaryNewes():Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.get(`${this.API_URL}getSumaryNewes`, {headers: header});
    }

    public getDetailNews(id: number):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        return this.http.get(`${this.API_URL}getDetailNews/${id}`, {headers: header});
    }

    public getNewes():Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.get(`${this.API_URL}getNewes`, {headers: header});
    }

    public createNewsByAdmin(news: News):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL}createNewsByAdmin`, news, {headers: header});
    }

    public updateNewsByAdmin(news: News):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL}updateNewsByAdmin`, news, {headers: header});
    }

    public deleteNewsByAdmin(id: number):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        
        return this.http.get(`${this.API_URL}deleteNewsByAdmin/${id}`, {headers: header});
    }
}