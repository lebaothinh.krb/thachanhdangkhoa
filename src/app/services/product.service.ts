import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Product } from '../shared/models/product';
// import { environment } from '../../environments/environment';
@Injectable()
export class ProductService {

    public API_URL = environment.API_URL+'/products/';

    constructor(private http: HttpClient) {
    }

    public getAllProducts(index: number){
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.get(`${this.API_URL}/getAllProducts/${index}`, { headers: header });
    }

    public getProduct(searchingKey = '', topic = 1, type = 1):Observable<any> {
        let header = new HttpHeaders();
        // header = header.append('Authorization', localStorage.getItem('token'));
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        let formData = {
            searchingKey: searchingKey,
            topic: topic,
            type: type
        }
        return this.http.post(`${this.API_URL}/getProducts`, formData, { headers: header });
        // return this.http.get(`${this.API_URL}getProducts`, {headers: header});
    }

    public createProductByAdmin(product: Product):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL}createProductByAdmin`, product, {headers: header});
    }
    
    public updateProductByAdmin(product: Product):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL}updateProductByAdmin`, product, {headers: header});
    }

    public deleteProductByAdmin(id: number):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        
        return this.http.get(`${this.API_URL}deleteProductByAdmin/${id}`, {headers: header});
    }
}