import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// import { environment } from '../../environments/environment';
@Injectable()
export class UploadService {

    public API_URL = environment.API_URL+'/Uploads/';
    
    constructor(private http: HttpClient) {
    }

    public uploadImgEditor(formData): Observable<any>{
        let header = new HttpHeaders();
        return this.http.post(`${this.API_URL}updateImgEditor`, formData, {headers: header});
    }

    public uploadImgProductByAdmin(formData):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        return this.http.post(`${this.API_URL}updateImgProductByAdmin`, formData, {headers: header});
    }

    public uploadImgNewsByAdmin(formData):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        return this.http.post(`${this.API_URL}updateImgNewsByAdmin`, formData, {headers: header});
    }
}