import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class ProcessService {

    public API_URL = environment.API_URL;
    
    private tasks: Array<number> = [];

    constructor() {
    }

    get getTask(){
        return this.tasks
    }

    public addTask(): number{
        let id: number = Math.max(...this.tasks)+1;
        this.tasks.push(id);
        return id;
    }

    public removeTask(id: number){
        this.tasks = this.tasks.filter(t => t != id);
    }
}