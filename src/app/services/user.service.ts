import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MailForm } from '../shared/models/mailForm';
// import { environment } from '../../environments/environment';
@Injectable()
export class UserService {

    public API_URL = environment.API_URL+'/users/';

    constructor(private http: HttpClient) {
    }

    public signIn(formData): Observable<Object> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL}signin`,formData, {headers: header});
        // return this.http.get(`https://5df35808f9e7ae00148010e9.mockapi.io/products`, {headers: header});
    }

    public registerEmail(email: string):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.get(`${this.API_URL}registerEmail/${email}`, {headers: header});
    }

    public sendInfoByAdmin(mailForm: MailForm):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        return this.http.post(`${this.API_URL}sendInfoByAdmin`,mailForm, {headers: header});
    }
}