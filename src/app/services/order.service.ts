import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { OrderByRequest, Order } from '../shared/models/order';
import { Product } from '../shared/models/product';
// import { environment } from '../../environments/environment';
@Injectable()
export class OrderService {

    public API_URL1 = environment.API_URL+'/orders/';
    public API_URL2 = environment.API_URL+'/ordersByRequest/';

    constructor(private http: HttpClient) {
    }

    public onOrder(formData: any):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        formData.htmlImgs = "";
        formData.products.forEach((p: Product) => {
            formData.htmlImgs += `<a href="${encodeURI("http://192.168.0.106:4200/products;searchingKey="+p.productName+";topic=1;type=1")}"><img
            style="height:130px; border: .5px solid #F1F1F1;" src="${p.imgsToList[0]}" /></a>`;
        })
        
        return this.http.post(`${this.API_URL1}onOrder`,formData, {headers: header});
    }

    public onOrderByRequest(formData: OrderByRequest):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL2}onOrderByRequest`, formData, {headers: header});
    }

    public getOrdersByAdmin():Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.get(`${this.API_URL1}getOrdersByAdmin`, {headers: header});
    }

    public getOrdersByRequestByAdmin():Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.get(`${this.API_URL2}getOrdersByRequestByAdmin`, {headers: header});
    }

    public updateOrderByAdmin(order: Order):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL1}updateOrderByAdmin`, order, {headers: header});
    }

    public updateOrderByRequestByAdmin(order: OrderByRequest):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post(`${this.API_URL2}updateOrderByRequestByAdmin`, order, {headers: header});
    }

    public deleteOrderByAdmin(id: number):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        
        return this.http.get(`${this.API_URL1}deleteOrderByAdmin/${id}`, {headers: header});
    }

    public deleteOrderByRequestByAdmin(id: number):Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        
        return this.http.get(`${this.API_URL2}deleteOrderByRequestByAdmin/${id}`, {headers: header});
    }

    public updateOrderStatusByAdmin(id, status): Observable<any>{
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        
        return this.http.get(`${this.API_URL1}updateOrderStatusByAdmin/${id}/${status}`, {headers: header});
    }

    public updateOrderStatusByRequestByAdmin(id, status): Observable<any>{
        let header = new HttpHeaders();
        header = header.set('Authorization', `Bearer ${localStorage.getItem('jwt')}`);
        header = header.set('Content-Type', 'application/json; charset=utf-8');
        
        return this.http.get(`${this.API_URL2}updateOrderStatusByRequestByAdmin/${id}/${status}`, {headers: header});
    }
}